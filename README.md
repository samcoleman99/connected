###***Connected* by Steven Shivaro**

---

### Introduction
---
In the article, *Connected* by Steven Shaviro, He outlays the consequences, both positive and negative, of connections. The basis of his argument lies not in business connections or material connections, but in technology. In the opening paragraph of his book, Shaviro states, “Every connection has a price”. He uses that concept to draw the seemingly mistaken course we have taken as a society to get everybody connected – again through technology. He introduces the idea of the 24/7 nature of our technology as a toll on humanity, and almost as a time bomb waiting to go off. Shaviro brings into play the near impossibility of getting off the network. This is also connected to the way one would write a paper. What one uses for how they write is important, and it affects how they write, the format they write in, and maybe even what they write about.

### Analysis
---
Shaviro spends almost his entire either article comparing different types of technology, or comparing technology to something else. At one point, Shaviro brings up the difference between TV shows and movies. The point is brought up that TV can be used as ambiance. One can use it as background, turn it on while doing chores, or simply just lounge on the couch and watch. For movies, it is slightly different. Movies demand a new level of attention. They require a higher level of attentiveness to fully understand the plot, the characters, and more. I can feel this working in my life as well when I use different word processors. The one I’m using right now, Bitbucket, is more like a movie. The things I can do with it affect how much attention I’m putting into things that I would not have even thought to pay attention to otherwise. Things like different formats I could use, the style in which I wanted my paper to be, and the new lines of code I could input into the system to make it different.

This all changes depending on what I am doing. If I am taking notes during class, I tend to use Google Docs more often. The speed and efficiency of the document allows me to annotate, edit, and take notes faster than any other word processor I have. If I am writing a paper for class, I tend to use Microsoft Word to write it because I know that I have more options when using Word. I can do more things with my text, and I have much more versatility at my fingertips. The ways I choose my word processors have evolved as well. I have grown from my elementary school years, where I admittedly did not use much technology at all. I wrote all of my assignments without the aid of any technology. Rather, I simply wrote all of my papers and assignments on simple notebook paper. Throughout middle school and high school, Google Docs was my school-wide use for how to submit and write papers. The cloud capabilities and the seamless efficiency of the network worked well for me. Now that I am in college and the network has switched over to Microsoft Office, I still find myself using Google Docs if I ever need to type notes, or type something fast. My technology has evolved, especially when using word processors. 

### Conclusion
---
> *"The network is the great Outside that always surrounds and envelops me.
>But it is also the Inside: its alien circuitry is what I find when I look 
>deeply within myself."* 

We have seemingly unlimited choice. This quote above is very true, even to me. With technology, whether we’re talking about word processors or artificial intelligence, it truly is the great Outside where we can experience, search, edit, and make whatever we could possibly think of. Where as in the past we would find it common to have but a couple of options to choose from, now we can choose from our heart’s desire. We can get just about anything we could want from the internet, or at least come as close as we can get without tangibly experiencing it.